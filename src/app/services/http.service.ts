import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs/index';

@Injectable()
export class HttpService {

  constructor(private router: Router,
              private http: HttpClient) {
  }

  get(url: string, params: any = {}): Observable<HttpResponse<Object>> {
    return this.http.get<HttpResponse<Object>>(url, this.addOptions(this.toHttpParams(params)))
      .catch(error => this.handleError(error));
  }

  post(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.post<HttpResponse<Object>>(url, body, this.addOptions())
      .catch(error => this.handleError(error));
  }

  patch(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.patch<HttpResponse<Object>>(url, body, this.addOptions())
      .catch(error => this.handleError(error));
  }

  put(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.put<HttpResponse<Object>>(url, body, this.addOptions())
      .catch(error => this.handleError(error));
  }

  delete(url: string): Observable<HttpResponse<Object>> {
    return this.http.delete<HttpResponse<Object>>( url, this.addOptions())
      .catch(error => this.handleError(error));
  }

  private toHttpParams(params) {
    params = this.checkParams(params);

    return Object.getOwnPropertyNames(params)
      .reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }

  private checkParams(obj) {
    return JSON.parse(JSON.stringify(obj,
      function (k, v) {
        if (v === null) {
          return undefined;
        }
        return v;
      }
    ));
  }

  private addOptions(params?: HttpParams) {
    const options = {};
    if (params) {
      options['params'] = params;
    }

    options['observe'] = 'response';
    return options;
  }

  private handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        this.router.navigate(['']);
      }

      return throwError(error);
    }
  }
}
