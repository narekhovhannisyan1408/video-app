import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';
import { MaterialModule } from './material.module';
import { HomeService } from '../components/home/home.service';
import { HttpService } from '../services/http.service';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { YoutubePlayerModule } from 'ng2-youtube-player';

// shared directives

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    AgmCoreModule,
    TranslateModule,
    InfiniteScrollModule,
    YoutubePlayerModule
  ],
  declarations: [],
  entryComponents: [],
  exports: [
    CommonModule,
    FileUploadModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    AgmCoreModule,
    TranslateModule,
    InfiniteScrollModule,
    YoutubePlayerModule
  ],
  providers: [
    DatePipe,
    HomeService,
    HttpService
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
