export class HomeModel {
  constructor(
    public author_name: string,
    public author_url: string,
    public height: number,
    public html: string,
    public provider_name: string,
    public provider_url: string,
    public thumbnail_height: number,
    public thumbnail_url: string,
    public thumbnail_width: number,
    public title: string,
    public type: string,
    public url: string,
    public version: string,
    public width: number,
    public isPlay: boolean
  ) {}
}
