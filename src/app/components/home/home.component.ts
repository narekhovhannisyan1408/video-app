import { Component, OnInit } from '@angular/core';

import { HomeService } from './home.service';
import { appConfig } from '../../app.config';
import { map } from 'rxjs/internal/operators';
import { HomeModel } from './home.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  keys = [
    'sw-l5SW5hik',
    '2jSWMme12ik',
    'p8npDG2ulKQ',
    'pl-DjiO8das',
    'CJRw18MtRPg',
    'Wpm07-BGJnE',
    'xf4iv4ic70M',
    '6Q0AazVu1Tc',
    'Xyb1fsPG-Xk',
    'kG41zm8HGSE',
    'RxvcH25WThg',
    'df7PZIVe1lw',
    'sYvH7Y16iUM',
    'juTa0fPI22M',
    '2KuqjW0WtZg',
    'dKccvk36atQ',
    'Duc3F700lgE',
    'TI5bEf-BULU',
    'sxyotaytAS0',
    'UZ47aQFp2TQ',
    'qD2yyikDcDw',
    'O4_JNAFClFk',
    'iJz5jURaEBc',
    'RBbkCEHBw_I',
    'CX11yw6YL1w'
  ];
  data: Array<HomeModel> = [];
  oldIndex: number;
  private player;

  constructor(private svc: HomeService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    for (let i = 0; i < this.keys.length; i++) {
      this.svc.getData(`${appConfig.titleUrl}${appConfig.videoUrl}${this.keys[i]}`)
        .pipe(map(response => {
          return response.body;
        })).subscribe((res: HomeModel) => {
        this.data.push(res);
      });
    }
  }

  playYTVideo(index) {
    this.data[index].isPlay = true;
    if (this.oldIndex ||  this.oldIndex === 0) {
      this.data[this.oldIndex].isPlay = false;
    }
    this.oldIndex = index;
  }

  savePlayer(player) {
    this.player = player;
    this.player.playVideo();
  }

  onStateChange(e) {
  }
}
