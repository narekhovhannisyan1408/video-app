import { Injectable } from '@angular/core';

import {HttpService} from '../../services/http.service';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()
export class HomeService {

  dataChanged = new BehaviorSubject<any>(null);
  showcaseIdChanged = new BehaviorSubject<any>(null);

  constructor(private http: HttpService) { }

  getData(url: string,  params: any = {}): Observable<any> {
    return this.http.get('/' + url, params);
  }

  addData(url: string, body): Observable<any> {
    return this.http.post('/' + url, body);
  }
  editData(url: string, body): Observable<any> {
    return this.http.put('/' + url, body);
  }
  editPatchData(url: string, body): Observable<any> {
    return this.http.patch('/' + url, body);
  }
  deleteData(url: string): Observable<any> {
    return this.http.delete('/' + url);
  }

  get dataListAsync() {
    return this.dataChanged.asObservable();
  }

  setDataList(e) {
    this.dataChanged.next(e);
  }
  get showcaseIdAsync() {
    return this.showcaseIdChanged.asObservable();
  }

  setShowcaseId(e) {
    this.showcaseIdChanged.next(e);
  }
}
